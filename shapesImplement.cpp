#include <iostream>
#include <vector>

#include "shapes.h"

int main() {

	std::vector<Shape*> pointers;
	Square* sq_pointer = new Square(2);
	Rectangle* rec_pointer = new Rectangle(2, 6);
	Circle* cir_pointer = new Circle(1);



	pointers.push_back(sq_pointer);
	pointers.push_back(rec_pointer);
	pointers.push_back(cir_pointer);

	for (int i =0; i < pointers.size(); i++) {
		std::cout << pointers[i]->description() << std::endl;
	}

	delete sq_pointer;
	delete rec_pointer;
	delete cir_pointer;


}
